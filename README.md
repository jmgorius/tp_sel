# TP SEL - Victor Careil, Jean-Michel Gorius

Ce dépôt contient le code source du TP de *Systèmes d'Exploitations Linux* (SEL).

Le dernier challenge validé est le challenge 4. Le challenge bonus est entièrement écrit et fonctionne partiellement. En effet, la modification dynamique du code d'une fonction appelée par un code multithreadé se déroule sans problème sur deux de nos machines de test, mais boucle en attente d'un signal d'arrêt sur une autre machine. Nous n'expliquons pas ce phénomène.

Dans le cadre du challenge 5, nous avons constaté un comportement intéressant du programme tracé. Ce dernier est plus efficace sans aucune modification lorsque le processeur est soumis à une forte charge d'activité (à l'aide de la commande `stress` par exemple). Après une inspection détaillée de l'exécution à l'aide de `perf` et `mutrace`, il s'avère que, dans le cas d'une importante charge de calcul, le *scheduler* du noyau Linux tend à ne faire s'exécuter qu'un seul thread du programme tracé à la fois. Ce comportement réduit drastiquement la quantité de changement de contexte entre threads, réduisant par ainsi la latence induite par les changements de propriétaire du *mutex*.

## Pour tester

* Lancer `build.sh` 
* Lancer dans un premier terminal `run_test_prog.sh`. Entrer `1` pour voir les stats actuelles, et `0` pour quitter.
* Lancer `run_optimizer.sh` dans un deuxième terminal.
* Le programme de test est maintenant plus rapide