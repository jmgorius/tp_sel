cmake_minimum_required(VERSION 3.10)
project(tp_sel)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_CXX_FLAGS "-ldl -Wall -Wextra")

set(SOURCE_FILES
        main.cpp
        ProcessHelper.h
        Utilities.h
        ProcessHelper.cpp)

add_executable(tpsel ${SOURCE_FILES})

add_executable(testprog
        test_prog/main.c)

add_executable(multithread_prog
        multithread_prog/main.c)

target_link_libraries(multithread_prog PUBLIC pthread)
