#include <sys/mman.h>
#include "ProcessHelper.h"

uint64_t ProcessHelper::GetFunctionAddress(const std::string &functionName) const
{
    char cmd[MAX_CMD_LENGTH];
    sprintf(cmd, R"(nm %s | grep "\b%s\b")", binaryPath.c_str(), functionName.c_str());

    FILE* fp = popen(cmd, "r");
    assert(fp);
    uint64_t func_addr;
    fscanf(fp, "%lx", &func_addr);
    pclose(fp);

    return func_addr;
}

uint64_t ProcessHelper::GetLibcFunctionAddress(const std::string &name)
{
    uint64_t func_offset;
    {
        char cmd[2048];
        sprintf(cmd, R"(objdump --dynamic-syms %s | grep "\b%s\b")", libcPath.c_str(), name.c_str());

        FILE* fp = popen(cmd, "r");
        assert(fp);
        fscanf(fp, "%lx", &func_offset);
        pclose(fp);
    }

    return libcBaseAddr + func_offset;
}

std::map<pid_t, std::unique_ptr<class ThreadHelper>> ProcessHelper::CreateThreadHelpers()
{
    auto result = std::map<pid_t, std::unique_ptr<ThreadHelper>>();
    for(auto& tid : tids)
    {
        result[tid] = std::make_unique<ThreadHelper>(*this, tid);
    }
    return result;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void ThreadHelper::Attach()
{
    std::clog << "Attaching to thread " << tid << "...";
    if (ptrace(PTRACE_ATTACH, tid, NULL, NULL))
    {
        std::clog << " Failed: " << strerror(errno) << std::endl;
        exit(1);
    }
    else
    {
        std::clog << " Success!" << std::endl;
    }
}

void ThreadHelper::Detach()
{
    SetRegisters();
    std::clog << "Detaching thread " << tid << "...";
    if (ptrace(PTRACE_DETACH, tid, NULL, NULL))
    {
        std::clog << " Failed: " << strerror(errno) << std::endl;;
        exit(1);
    }
    else
    {
        std::clog << " Success!" << std::endl;
    }
}

void ThreadHelper::Resume()
{
    SetRegisters();
    std::clog << "Resuming thread " << tid << "...";
    if (ptrace(PTRACE_CONT, tid, NULL, NULL))
    {
        std::clog << " Failed: " << strerror(errno) << std::endl;;
        exit(1);
    }
    else
    {
        std::clog << " Success!" << std::endl;
    }
}

void ThreadHelper::SingleStep()
{
    SetRegisters();
    if (ptrace(PTRACE_SINGLESTEP, tid, NULL, NULL))
    {
        std::clog << "PTRACE_SINGLESTEP failed for thread " << tid << ": " << strerror(errno) << std::endl;
        exit(1);
    }
}

void ThreadHelper::Wait()
{
    std::clog << "Waiting...";
    int status;
    if (waitpid(tid, &status, 0) == -1)
    {
        std::clog << " Failed: " << strerror(errno) << std::endl;
        exit(1);
    }

    if (WIFSTOPPED(status))
    {
        std::clog << " Received " << strsignal(WSTOPSIG(status)) << std::endl;
    }
    else
    {
        std::clog << "Failed with status " << status << std::endl;
    }
    GetRegisters();
}

uint64_t ThreadHelper::CallFunction(
        const std::string& name,
        uint64_t functionAddress,
        std::initializer_list<FunctionArgument> args)
{
    uint64_t startRip = regs.rip;
    std::ostringstream oss;
    oss << "Calling "
        << name
        << "@"
        << (void*) functionAddress;
    ExploitScope Exploit(oss.str(), *this, 0x1000, startRip);

    {
        MemoryScopeAccessor accessor(*this);

        uint8_t posixCallCode[] = {
                0xff, 0xd0,
                0xcc
        };
        accessor.WriteAbs(regs.rip, sizeof(posixCallCode), posixCallCode);

        int index = 0;
        for (auto& arg : args)
        {
            auto& argReg = index == 0 ? regs.rdi : index == 1 ? regs.rsi : regs.rdx;
            index++;
            if (arg.IsPtr())
            {
                // Allocate 64 bits on the stack
                regs.rsp -= sizeof(void*);
                // Set the arg address
                argReg = regs.rsp;
                arg.ptr->ptrAddress = regs.rsp;
                // Write the default value
                accessor.WriteAtAbs(regs.rsp, arg.data);
            }
            else
            {
                argReg = arg.data;
            }
        }
        regs.rax = functionAddress;
    }

    Resume();
    Wait();

    MemoryScopeAccessor accessor(*this);
    assert(regs.rip == startRip + 3);
    for (auto& arg : args)
    {
        if (arg.IsPtr())
        {
            // Load the results
            arg.ptr->ptrData = accessor.ReadAtAbs<uint64_t>(arg.ptr->ptrAddress);
        }
    }
    return regs.rax;
}

uint64_t ThreadHelper::AllocateCodeCache(uint64_t pageCount)
{
    FunctionArgument::PtrResult memPtr;
    uint64_t memalign = CallLibcFunction("posix_memalign", {memPtr, PAGE_SIZE, pageCount * PAGE_SIZE});
    uint64_t mprotect = CallLibcFunction("mprotect", {memPtr.ptrData,
                                                      pageCount * PAGE_SIZE,
                                                      PROT_EXEC | PROT_READ | PROT_WRITE});

    std::clog << "posix_memalign: " << (memalign ? "Error" : "Success") << " " << memalign << std::endl;
    std::clog << "mprotect: " << (mprotect ? "Error" : "Success") << " " << mprotect << std::endl;

    return memPtr.ptrData;
}

void ThreadHelper::GetRegisters()
{
    if (ptrace(PTRACE_GETREGS, tid, NULL, &regs))
    {
        std::clog << "PTRACE_GETREGS failed for tid " << tid << ": " << strerror(errno) << std::endl;
        exit(1);
    }
}

void ThreadHelper::SetRegisters()
{
    if (ptrace(PTRACE_SETREGS, tid, NULL, &regs))
    {
        std::clog << "PTRACE_SETREGS failed for tid " << tid << ": " << strerror(errno) << std::endl;
        exit(1);
    }
}