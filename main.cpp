#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <sys/ptrace.h>
#include <wait.h>
#include <cstring>
#include <cstdint>
#include <unistd.h>
#include <sys/user.h>
#include <sys/mman.h>
#include <iostream>
#include "ProcessHelper.h"

int main(int argc, char* argv[])
{
    if(argc != 4)
    {
        std::clog << "Usage: ./tp_sel pid function_to_trap global_variable" << std::endl;
        return 1;
    }
    pid_t pid = atoi(argv[1]);
    if (!pid) scanf("%d", &pid);
    char* funcToTrapName = argv[2];
    char* varToIncrName = argv[3];

    ProcessHelper process(pid);

    std::clog << "PID: " << pid << std::endl;
    std::clog << "TIDs:";
    for(auto& tid : process.tids)
    {
        std::clog << " " << tid;
    }
    std::clog << std::endl;
    std::clog << "Executable path: " << process.binaryPath << std::endl;

    uint64_t funcToTrapAddr = process.GetFunctionAddress(funcToTrapName);
    uint64_t varToIncAddr = process.GetFunctionAddress(varToIncrName);
    std::clog << "Function to trap address: " << (void*) funcToTrapAddr << std::endl;
    std::clog << "Variable to incr address: " << (void*) varToIncAddr << std::endl;
    std::clog << "Program .text offset: " << (void*) process.memoryOffset << std::endl;

    // Attach to the threads & wait
    for (auto& [tid, thread] : process.threads)
    {
        thread->Attach();
        thread->Wait();
    }

    // Write the trap
    MemoryScopeAccessor(process).WriteRel(funcToTrapAddr, {0xCC});

    // Resume all the threads (except the main one as it will never go to the trap)
    for (auto& [tid, thread] : process.threads)
    {
        if (tid != process.pid)
        {
            thread->Resume();
        }
    }

    // Wait for all the threads to go to the trap
    for (auto& [tid, thread] : process.threads)
    {
        if (tid != process.pid)
        {
            thread->Wait();
            std::clog << "rip thread " << std::dec << tid << ": " << (void*) thread->regs.rip << std::endl;
        }
    }

    std::clog << "libc path: " << Utilities::GetLibcPath(pid) << std::endl;

    {
        auto& mainThread = *process.threads.at(process.pid);
        // Allocate 4 pages
        uint64_t codeCacheAddr = mainThread.AllocateCodeCache(4);
        std::clog << "cache: " << (void*) codeCacheAddr << std::endl;

        // Compile the code
        std::vector<uint8_t> code;
        std::ostringstream codeStream;
        codeStream << R"_(
void incr()
{
    int* tmp = (int*)0x)_" << std::hex << varToIncAddr + process.memoryOffset << R"_(;
    asm("lock incl %0" : "=m"(*tmp) : "m"(*tmp) : "memory");
}
        )_";
        std::clog << "Code:" << std::endl << codeStream.str() << std::endl;
        Utilities::Compile(codeStream.str(), code);

        // Write it in the code cache & add a trampoline
        {
            MemoryScopeAccessor accessor(process);
            accessor.PrintAbs(codeCacheAddr);
            accessor.WriteAbs(codeCacheAddr, code);
            accessor.PrintAbs(codeCacheAddr);

            // trampoline
            accessor.WriteRel(funcToTrapAddr, {0x48, 0xb8});
            accessor.WriteAtRel<uint64_t>(funcToTrapAddr + 2, codeCacheAddr);
            accessor.WriteRel(funcToTrapAddr + 10, {0xff, 0xe0});
            accessor.PrintRel(funcToTrapAddr);
        }
    }

    // Detach
    for(auto& [tid, thread] : process.threads)
    {
        if(tid != process.pid)
        {
            // Go back one instruction before the trap. Not needed for the main thread as it does not have a trap
            thread->regs.rip--;
        }
        std::clog << "rip thread " << std::dec << tid << ": " << (void*) thread->regs.rip << std::endl;
        thread->Detach();
    }

    return 0;
}