#pragma once

#include <wait.h>
#include <string>
#include <sys/ptrace.h>
#include <cstring>
#include <sys/user.h>
#include <iostream>
#include <iomanip>
#include <optional>
#include <map>
#include "Utilities.h"

// An argument of a function to call in the attached process.
// Can either be a ptr (allocated on the stack) or a value.
struct FunctionArgument
{
    struct PtrResult
    {
        // The ptr
        uint64_t ptrAddress = 0;
        // The value at the ptr address after the call
        uint64_t ptrData = 0;
        PtrResult() = default;
    };
    enum class EType
    {
        Value,
        Ptr
    };

    EType const type;
    uint64_t const data;
    PtrResult *const ptr;

    // Create a value argument
    FunctionArgument(uint64_t data)
        : type(EType::Value), data(data), ptr(nullptr)
    {
    }

    // Create a ptr argument. data: default value of the pointed data
    FunctionArgument(PtrResult& result, uint64_t data = 0)
        : type(EType::Ptr), data(data), ptr(&result)
    {
    }

    inline bool IsPtr() const { return type == EType::Ptr; }
};

// Class handling a process
class ProcessHelper
{
public:
    // The process pid
    const pid_t pid;
    // The threads pids
    const std::vector<pid_t> tids;
    // All the threads
    const std::map<pid_t, std::unique_ptr<class ThreadHelper>> threads;
    // The path of the executable. Useful to eg get the address of a function
    const std::string binaryPath;
    // The offset of the memory (ASLR)
    const uint64_t memoryOffset;
    // The offset of the libc
    const uint64_t libcBaseAddr;
    // The path to the libc
    const std::string libcPath;

    ProcessHelper(pid_t pid)
        : pid(pid)
        , tids(Utilities::GetTIDs(pid))
        , threads(CreateThreadHelpers())
        , binaryPath(Utilities::GetBinaryPath(pid))
        , memoryOffset(Utilities::GetMemoryOffset(pid))
        , libcBaseAddr(Utilities::GetLibcBaseAddress(pid))
        , libcPath(Utilities::GetLibcPath(pid))
    {
    }

public:
    // Get the address of a function in the binary
    uint64_t GetFunctionAddress(const std::string& functionName) const;

    // Get the address of a libc function
    uint64_t GetLibcFunctionAddress(const std::string& name);

private:
    std::map<pid_t, std::unique_ptr<class ThreadHelper>> CreateThreadHelpers();
};

// Class handling a single thread
class ThreadHelper
{
public:
    // The process owning that thread
    ProcessHelper& process;
    // The tid of that thread
    const pid_t tid;
    // The current registers of that thread. Loaded after every Wait and saved back after every Resume
    user_regs_struct regs;

    ThreadHelper(ProcessHelper& Process, pid_t tid)
        : process(Process)
        , tid(tid)
    {
    }

public:
    // Attach to the thread
    void Attach();
    // Detach
    void Detach();

    // Resume the thread (PTRACE_CONT)
    void Resume();
    // Do a single step (PTRACE_SINGLESTEP)
    void SingleStep();
    // Wait for the thread (waitpid(tid))
    void Wait();

public:
    // Calls a function in the thread. Resumes it, but restores its state after
    uint64_t CallFunction(
            const std::string& debugName,
            uint64_t functionAddress,
            std::initializer_list<FunctionArgument> args);
    uint64_t CallLibcFunction(
            const std::string& name,
            std::initializer_list<FunctionArgument> args)
    {
        return CallFunction(name, process.GetLibcFunctionAddress(name), args);
    }

    // Allocate a code cache (posix_memalign + mprotect)
    uint64_t AllocateCodeCache(size_t pageCount);

private:
    // Load the registers from the thread into regs
    void GetRegisters();
    // Set the thread registers to regs
    void SetRegisters();
};

// A memory save: stores the data & where it was (offset)
struct MemorySave
{
    size_t size = 0;
    uint64_t offset = 0;
    uint8_t *buffer = nullptr;

    void Init(size_t inSize, uint64_t inOffset)
    {
        assert(!buffer);
        size = inSize;
        offset = inOffset;
        buffer = new uint8_t[size];
    }
    ~MemorySave()
    {
        delete[] buffer;
    }

};

// Automatically close the memory file when deleted.
// All functions have 2 versions:
// - FuncAbs for absolute address
// - FuncRel for relative address (relative to the process offset)
class MemoryScopeAccessor
{
public:
    MemoryScopeAccessor(const ProcessHelper& process)
        : file(Utilities::GetMemoryFile(process.pid)), memoryOffset(process.memoryOffset)
    {
    }
    MemoryScopeAccessor(const ThreadHelper& thread)
        : MemoryScopeAccessor(thread.process)
    {
    }
    ~MemoryScopeAccessor()
    {
        fclose(file);
    }

    template<typename T>
    T ReadAtAbs(uint64_t offset) const
    {
        T value;
        fseek(file, offset, SEEK_SET);
        fread(&value, sizeof(T), 1, file);
        return value;
    }
    template<typename T>
    T ReadAtRel(uint64_t offset) const
    {
        return ReadAtAbs<T>(memoryOffset + offset);
    }

    template<typename T>
    void WriteAtAbs(uint64_t offset, T value)
    {
        WriteAbs(offset, sizeof(T) / sizeof(uint8_t), (uint8_t *) &value);
    }
    template<typename T>
    void WriteAtRel(uint64_t offset, T value)
    {
        return WriteAtAbs(memoryOffset + offset, value);
    }

    void WriteRel(uint64_t offset, size_t size, uint8_t *data)
    {
        WriteAbs(memoryOffset + offset, size, data);
    }
    template<template<typename> typename T = std::initializer_list>
    void WriteRel(uint64_t offset, const T<uint8_t>& data)
    {
        WriteAbs(memoryOffset + offset, data);
    }
    template<template<typename> typename T = std::initializer_list>
    void WriteAbs(uint64_t offset, const T<uint8_t>& data)
    {
        fseek(file, offset, SEEK_SET);
        for (auto Elem : data)
            fputc(Elem, file);
    }
    void WriteAbs(uint64_t offset, size_t size, uint8_t *data)
    {
        fseek(file, offset, SEEK_SET);
        for (size_t i = 0; i < size; i++)
            fputc(data[i], file);
    }

    void GetSaveRel(size_t size, uint64_t offset, MemorySave& outSave) const
    {
        GetSaveAbs(size, memoryOffset + offset, outSave);
    }
    void GetSaveAbs(size_t size, uint64_t offset, MemorySave& outSave) const
    {
        outSave.Init(size, offset);
        fseek(file, offset, SEEK_SET);
        fread(outSave.buffer, sizeof(uint8_t), size, file);
    }

    void LoadSave(const MemorySave& save)
    {
        fseek(file, save.offset, SEEK_SET);
        fwrite(save.buffer, sizeof(uint8_t), save.size, file);
    }

    void PrintRel(uint64_t offset, size_t size = 2048)
    {
        PrintAbs(memoryOffset + offset, size);
    }
    void PrintAbs(uint64_t offset, size_t size = 2048)
    {
        // Assuming sizeof(char) = 1
        char *buffer = (char *) malloc(size);
        fseek(file, offset, SEEK_SET);
        fread(buffer, sizeof(char), size, file);

        std::clog << "\033[1;30m" << "@" << (void *) offset << ": " << "\033[0;30m";
        for (size_t i = 0; i < size; i++) {
            std::clog << std::setfill('0') << std::setw(2) << std::hex << (buffer[i] & 0xff);
            if (i & 0x1) std::clog << " ";
        }
        std::clog << "\033[0m" << std::endl;
        free(buffer);
    }

private:
    FILE *const file;
    uint64_t const memoryOffset;
};

// Create a save of the memory & registers when constructed, and restore them when deleted.
class ExploitScope
{
public:
    ExploitScope(const std::string& name, ThreadHelper& thread, size_t size, uint64_t offsetAbs)
        : name(name), thread(thread)
    {
        regs = thread.regs;
        MemoryScopeAccessor accessor(thread);
        accessor.GetSaveAbs(size, offsetAbs, save);
        std::clog << "\033[1;31m"
                  << "Exploit "
                  << "\033[1;30m"
                  << name
                  << "\033[1;31m"
                  << " started "
                  << "\033[1;30m"
                  << "@" << (void *) save.offset
                  << "[" << save.size << "]"
                  << "\033[0m"
                  << std::endl;
    }

    ~ExploitScope()
    {
        thread.regs = regs;
        MemoryScopeAccessor accessor(thread);
        accessor.LoadSave(save);
        std::clog << "\033[1;31m"
                  << "Exploit "
                  << "\033[1;30m"
                  << name
                  << "\033[1;31m"
                  << " ended"
                  << "\033[0m"
                  << std::endl;
    }

private:
    const std::string name;
    ThreadHelper& thread;
    user_regs_struct regs;
    MemorySave save;
};