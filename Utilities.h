#pragma once

#include <wait.h>
#include <string>
#include <vector>
#include <cassert>
#include <zconf.h>
#include <dirent.h>

#define MAX_PATH_LENGTH 1024
#define MAX_CMD_LENGTH 2048

namespace Utilities
{
    inline std::string GetBinaryPath(pid_t pid)
    {
        char linkPath[MAX_PATH_LENGTH];
        sprintf(linkPath, "/proc/%d/exe", pid);

        char binPath[MAX_PATH_LENGTH];
        ssize_t pathLength = readlink(linkPath, binPath, MAX_PATH_LENGTH);
        assert(pathLength != -1);
        binPath[pathLength] = '\0';

        return std::string(binPath);
    }

    inline uint64_t GetMemoryOffset(pid_t pid)
    {
        char mapsFilename[MAX_PATH_LENGTH];
        sprintf(mapsFilename, "/proc/%d/maps", pid);

        FILE* fp = fopen(mapsFilename, "r");
        assert(fp);
        uint64_t offset;
        fscanf(fp, "%lx", &offset);
        fclose(fp);

        return offset;
    }

    inline std::string GetLibcPath(pid_t pid)
    {
        char mapsFilename[MAX_PATH_LENGTH];
        sprintf(mapsFilename, "/proc/%d/maps", pid);

        char libcPath[MAX_PATH_LENGTH];
        {
            char cmd[MAX_CMD_LENGTH];
            sprintf(cmd, "cat %s | grep libc | awk '{ print $6 }'", mapsFilename);

            FILE* fp = popen(cmd, "r");
            assert(fp);
            fscanf(fp, "%s", libcPath);
            pclose(fp);
        }

        return std::string(libcPath);
    }

    inline uint64_t GetLibcBaseAddress(pid_t pid)
    {
        char mapsFilename[MAX_PATH_LENGTH];
        sprintf(mapsFilename, "/proc/%d/maps", pid);

        uint64_t libcBaseAddr;
        {
            char cmd[MAX_CMD_LENGTH];
            sprintf(cmd, "cat %s | grep libc", mapsFilename);

            FILE* fp = popen(cmd, "r");
            assert(fp);
            fscanf(fp, "%lx", &libcBaseAddr);
            pclose(fp);
        }

        return libcBaseAddr;
    }

    inline FILE* GetMemoryFile(pid_t PID)
    {
        char memFilename[MAX_PATH_LENGTH];
        sprintf(memFilename, "/proc/%d/mem", PID);

        FILE* memFile = fopen(memFilename, "w+");
        assert(memFile);
        return memFile;
    }

    inline void Compile(const std::string& code, std::vector<uint8_t>& binOut)
    {
        {
            FILE* srcFile = fopen("/tmp/tmp.c", "w");
            assert(srcFile);
            fprintf(srcFile, "%s", code.c_str());
            fclose(srcFile);
        }

        {
            char cmd[MAX_CMD_LENGTH];
            sprintf(cmd, "gcc -O0 -c /tmp/tmp.c -o /tmp/tmp.o && echo \"Success\"");
            FILE* fp = popen(cmd, "r");
            char res[4096];
            fscanf(fp, "%s\n", res);
            assert(strcmp(res, "Success") == 0);
            pclose(fp);
        }

        system("objcopy --only-section=.text --output-target binary /tmp/tmp.o /tmp/tmp.bin");

        {
            FILE* binFile = fopen("/tmp/tmp.bin", "rb");
            fseek(binFile, 0, SEEK_END);
            size_t size = ftell(binFile);
            rewind(binFile);
            binOut.resize(size);
            fread(binOut.data(), sizeof(char), size, binFile);
            fclose(binFile);
        }
    }

    inline std::vector<pid_t> GetTIDs(pid_t pid)
    {
        std::vector<pid_t> tids;

        char taskPath[MAX_CMD_LENGTH];

        sprintf(taskPath, "/proc/%d/task/", (int) pid);

        if (auto* taskDir = opendir(taskPath))
        {
            while (auto* dir = readdir(taskDir))
            {
                pid_t tid;
                sscanf(dir->d_name, "%d", &tid);
                std::string name(dir->d_name);
                if (name !=  "." && name != "..")
                {
                    tids.push_back(tid);
                }
            }
            closedir(taskDir);
        }

        return tids;
    }
}