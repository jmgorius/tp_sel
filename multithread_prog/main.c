#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <zconf.h>

#define NUM_THREADS 8

// call this function to start a nanosecond-resolution timer
struct timespec timer_start()
{
    struct timespec start_time;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    return start_time;
}

// call this function to end a timer, returning nanoseconds elapsed as a long
uint64_t timer_end(struct timespec start_time)
{
    struct timespec end_time;
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    uint64_t diffInNanos = (end_time.tv_sec - start_time.tv_sec) * (uint64_t) 1e9 + (end_time.tv_nsec - start_time.tv_nsec);
    return diffInNanos;
}

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int val = 0;

volatile int stop = 0;

void incr()
{
//    int* tmp = &val;//(int*)0x5555555580c8;
//    asm("lock incl %0" : "=m"(*tmp) : "m"(*tmp) : "memory");
    pthread_mutex_lock(&mutex);
    val++;
    pthread_mutex_unlock(&mutex);
}

pthread_mutex_t safe_mutex = PTHREAD_MUTEX_INITIALIZER;
int safe_val = 0;

void safe_incr()
{
    pthread_mutex_lock(&safe_mutex);
    safe_val++;
    pthread_mutex_unlock(&safe_mutex);
}

void thread_func()
{
    while (!stop)
    {
        incr();
        safe_incr();
    }
}

int main(int argc, char* argv[])
{
    pthread_t threads[NUM_THREADS] = {};

    for (int i = 0; i < NUM_THREADS; i++)
    {
        if (pthread_create(&threads[i], NULL, (void* (*)(void*)) thread_func, NULL))
        {
            perror("pthread_create");
            exit(EXIT_FAILURE);
        }
    }

    int val_last = 0;
    struct timespec last_time = timer_start();

    int tmp = 1;
    while (tmp)
    {
        scanf("%d", &tmp);

        int val_now = val;

        uint64_t diff = timer_end(last_time);
        printf("Cycles since last time: %llu\n", diff);
        printf("Values since last time: %d\n", val_now - val_last);
        printf("Cycles/Values (Smaller = Better): %f\n\n", (double) diff / (val_now - val_last));

        val_last = val_now;
        last_time = timer_start();
    }

    stop = 1;

    for (int i = 0; i < NUM_THREADS; i++)
    {
        if (pthread_join(threads[i], NULL))
        {
            perror("pthread_join");
            exit(EXIT_FAILURE);
        }
    }

    printf("Value = %d, should be %d\n", val, safe_val);
    if (val == safe_val)
    {
        printf("Success!\n");
    }
    else
    {
        printf("Error!\n");
    }
}
